package com.app.sample.shop.model;

import java.io.Serializable;

/**
 * Created by shobo on 6/7/17.
 */

public class OrderProduct implements Serializable {

    long id;
    String name;
    long price;
    int quantity;
    int img;

    public OrderProduct() {

    }

    public OrderProduct(long id, long price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    public OrderProduct(long id, long price, int quantity, int img, String name) {

        this.id = id;
        this.price = price;
        this.quantity = quantity;
        this.img = img;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public long getPrice() {
        return (price);
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public int getQuantity() {
        return (quantity);
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

}
