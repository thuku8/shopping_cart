package com.app.sample.shop.data;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.app.sample.shop.model.SavedLocations;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by shobo on 6/20/17.
 */

public class SharedPreference {

    public static final String PREFS_NAME = "PRODUCT_APP";
    public static final String FAVORITES = "Product_Favorite";

    public SharedPreference() {
        super();
    }

    // This four methods are used for maintaining favorites.
    public void saveLocations(Context context, List<SavedLocations> locations) {
        SharedPreferences settings;
        Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(locations);

        editor.putString(FAVORITES, jsonFavorites);

        editor.commit();
    }

    public void addLocation(Context context, SavedLocations savedLocations) {
        List<SavedLocations> locations = getLocations(context);
        if (locations == null)
            locations = new ArrayList<SavedLocations>();
        locations.add(savedLocations);
        saveLocations(context, locations);
    }

    public void removeLocation(Context context, SavedLocations savedLocations, int position) {
        ArrayList<SavedLocations> locations = getLocations(context);
        if (locations != null) {
            locations.remove(locations.indexOf(locations.get(position)));
            saveLocations(context, locations);
        }
    }

    public ArrayList<SavedLocations> getLocations(Context context) {
        SharedPreferences settings;
        List<SavedLocations> locations;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(FAVORITES)) {
            String jsonFavorites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            SavedLocations[] favoriteItems = gson.fromJson(jsonFavorites,
                    SavedLocations[].class);

            locations = Arrays.asList(favoriteItems);
            locations = new ArrayList<SavedLocations>(locations);
        } else
            return null;

        return (ArrayList<SavedLocations>) locations;
    }
}