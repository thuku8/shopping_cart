package com.app.sample.shop.activities.SplashAndInitialScreens;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.app.sample.shop.R;
import com.app.sample.shop.data.Tools;
import com.app.sample.shop.font.MaterialDesignIconsTextView;
import com.app.sample.shop.view.kbv.KenBurnsView;

import java.util.Timer;
import java.util.TimerTask;

public class ActivitySplash extends Activity {

	public static final String SPLASH_SCREEN_OPTION = "com.app.sample.shop.SplashScreensActivity";
	public static final String SPLASH_SCREEN_OPTION_1 = "Fade in + Ken Burns";
	public static final String SPLASH_SCREEN_OPTION_2 = "Down + Ken Burns";
	public static final String SPLASH_SCREEN_OPTION_3 = "Down + fade in + Ken Burns";

	private KenBurnsView mKenBurns;
	private MaterialDesignIconsTextView mLogo;
	private TextView welcomeText,welcomeTextSlogan;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE); //Removing ActionBar
		//setContentView(R.layout.activity_splash_screen);

		// go to the stated activity
		Intent i = new Intent(ActivitySplash.this, SplashPagerActivity.class);
		startActivity(i);
		// kill current activity
		finish();

		//will be used if we want to add animations to the splash screen but for now use the above
		//setUpActivity();


        // for system bar in lollipop
        Tools.systemBarLolipop(this);
	}

	private void setUpActivity() {
		mKenBurns = (KenBurnsView) findViewById(R.id.ken_burns_images);
		mLogo = (MaterialDesignIconsTextView) findViewById(R.id.logo);
		welcomeText = (TextView) findViewById(R.id.welcome_text);
		welcomeTextSlogan = (TextView) findViewById(R.id.welcome_text_slogan);

		String category = SPLASH_SCREEN_OPTION_1;
		Bundle extras = getIntent().getExtras();
		if (extras != null && extras.containsKey(SPLASH_SCREEN_OPTION)) {
			category = extras.getString(SPLASH_SCREEN_OPTION, SPLASH_SCREEN_OPTION_1);
		}
		setAnimation(category);
		timerTaskLaunchSplashPager();
	}

	/** Animation depends on category.
	 * */
	private void setAnimation(String category) {
		if (category.equals(SPLASH_SCREEN_OPTION_1)) {
			mKenBurns.setImageResource(R.drawable.background_media);
			animation1();
		} else if (category.equals(SPLASH_SCREEN_OPTION_2)) {
			//mLogo.setTextColor(R.color.main_color_500);
			//mLogo.setTextColor(R.color.wallet_link_text_light);
			//mKenBurns.setImageResource(R.drawable.background_shop);
			animation2();
			animation3();
			animation4();
		} else if (category.equals(SPLASH_SCREEN_OPTION_3)) {
			mKenBurns.setImageResource(R.drawable.splash_screen_option_three);
			animation2();
			animation3();
		}
	}

	private void animation1() {
		ObjectAnimator scaleXAnimation = ObjectAnimator.ofFloat(mLogo, "scaleX", 5.0F, 1.0F);
		scaleXAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
		scaleXAnimation.setDuration(1200);
		ObjectAnimator scaleYAnimation = ObjectAnimator.ofFloat(mLogo, "scaleY", 5.0F, 1.0F);
		scaleYAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
		scaleYAnimation.setDuration(1200);
		ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(mLogo, "alpha", 0.0F, 1.0F);
		alphaAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
		alphaAnimation.setDuration(1200);
		AnimatorSet animatorSet = new AnimatorSet();
		animatorSet.play(scaleXAnimation).with(scaleYAnimation).with(alphaAnimation);
		animatorSet.setStartDelay(500);
		animatorSet.start();
	}

	private void animation2() {
		mLogo.setAlpha(1.0F);
		Animation anim = AnimationUtils.loadAnimation(this, R.anim.translate_top_to_center);
		mLogo.startAnimation(anim);
	}

	private void animation3() {
		ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(welcomeText, "alpha", 0.0F, 1.0F);
		alphaAnimation.setStartDelay(1000);
		alphaAnimation.setDuration(500);
		alphaAnimation.start();
	}

	private void animation4() {
//		ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(welcomeTextSlogan, "alpha", 0.0F, 1.0F);
//		alphaAnimation.setStartDelay(1000);
//		alphaAnimation.setDuration(500);
//		alphaAnimation.start();

        ObjectAnimator scaleXAnimation = ObjectAnimator.ofFloat(welcomeTextSlogan, "scaleX", 5.0F, 1.0F);
        scaleXAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        scaleXAnimation.setDuration(1200);
        ObjectAnimator scaleYAnimation = ObjectAnimator.ofFloat(welcomeTextSlogan, "scaleY", 5.0F, 1.0F);
        scaleYAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        scaleYAnimation.setDuration(1200);
        ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(welcomeTextSlogan, "alpha", 0.0F, 1.0F);
        alphaAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        alphaAnimation.setDuration(1200);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(scaleXAnimation).with(scaleYAnimation).with(alphaAnimation);
        animatorSet.setStartDelay(1200);
        animatorSet.start();
	}

    private void timerTaskLaunchSplashPager() {

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                // go to the main activity
                Intent i = new Intent(ActivitySplash.this, SplashPagerActivity.class);
                startActivity(i);
                // kill current activity
                finish();
            }
        };
        // Show splash screen for 3 seconds
        new Timer().schedule(task, 100);


    }
}
