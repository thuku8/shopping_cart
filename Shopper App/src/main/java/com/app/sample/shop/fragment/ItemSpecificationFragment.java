package com.app.sample.shop.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.sample.shop.R;
import com.app.sample.shop.font.MaterialDesignIconsTextView;
import com.app.sample.shop.font.MyTextView;

/**
 * Created by shobo on 5/18/17.
 */

public class ItemSpecificationFragment extends Fragment {

    private LinearLayout prodFeaturesLayout;
    private LinearLayout boxFeaturesLayout;
    private TextView instructions;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_item_specification, null);

        prodFeaturesLayout = (LinearLayout) rootView.findViewById(R.id.prodFeaturesLayout);
        boxFeaturesLayout = (LinearLayout) rootView.findViewById(R.id.boxFeaturesLayout);
        //instructions = (TextView) rootView.findViewById(R.id.instructions);

        String[] prod_features = getResources().getStringArray(R.array.prod_key_features);
        String[] box_features = getResources().getStringArray(R.array.prod_box_features);
        addProdKeyFeaturesList(prodFeaturesLayout, prod_features);
        addProdBoxFeaturesList(boxFeaturesLayout, box_features);

        return rootView;
    }

//    <LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
//    xmlns:app="http://schemas.android.com/apk/res-auto"
//    android:layout_width="match_parent"
//    android:layout_height="wrap_content"
//    android:orientation="horizontal"
//    android:paddingLeft="10dp"
//    android:paddingRight="10dp" >
//
//    <com.app.sample.shop.font.MaterialDesignIconsTextView
//            style="@style/TextViewAppearance.Title1"
//    android:layout_width="wrap_content"
//    android:layout_height="wrap_content"
//    android:gravity="center_vertical"
//    android:minHeight="48dp"
//    android:padding="8dp"
//    android:text="@string/material_icon_to_left"
//    android:textColor="@color/main_color_grey_700"
//    android:textSize="16sp" />
//
//    <com.app.sample.shop.font.RobotoTextView
//    android:id="@+id/textTitle"
//    style="@style/TextViewAppearance.Title1"
//    android:layout_width="match_parent"
//    android:layout_height="wrap_content"
//    android:layout_marginLeft="16dp"
//    android:gravity="center_vertical"
//    android:minHeight="48dp"
//    android:padding="8dp"
//    android:textColor="@color/main_color_grey_700"
//    android:textSize="16sp"
//    app:typeface="robotoRegular" />
//
//    </LinearLayout>

    private void addProdKeyFeaturesList(LinearLayout linearLayout, String[] title){
        for (int i = 0; i < title.length; i++) {
            LinearLayout ll = new LinearLayout(getActivity());
            ll.setOrientation(LinearLayout.VERTICAL);

            LinearLayout hl = new LinearLayout(getActivity());
            hl.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            hl.setOrientation(LinearLayout.HORIZONTAL);
            ll.addView(hl);

            //create a material design text icon
            MaterialDesignIconsTextView mdIconTxt = new MaterialDesignIconsTextView(getActivity());
            ViewGroup.LayoutParams params = mdIconTxt.getLayoutParams();
            if (params != null) {
                params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            } else{
                params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
            mdIconTxt.setLayoutParams(params);
            mdIconTxt.setMinHeight(30);
            mdIconTxt.setPadding(8,8,8,8);
            mdIconTxt.setGravity(Gravity.RIGHT);
            mdIconTxt.setText(getResources().getString(R.string.material_icon_to_left));
            mdIconTxt.setTextSize(16);
            mdIconTxt.setTextColor(getResources().getColor(R.color.main_color_grey_700));
            //get rid of deprecation
            mdIconTxt.setTextColor(ContextCompat.getColor(getActivity(), R.color.main_color_grey_700));

            MyTextView txtTitle = new MyTextView(getActivity());
            ViewGroup.LayoutParams txtParams = txtTitle.getLayoutParams();
            if (txtParams != null) {
                txtParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                txtParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            } else{
                txtParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
            //lets set the margins
            txtTitle.setGravity(Gravity.CENTER_VERTICAL);
            txtTitle.setMinHeight(30);
            txtTitle.setPadding(8,8,8,8);
            txtTitle.setTextSize(14);
            txtTitle.setTextColor(getResources().getColor(R.color.main_color_grey_700));
            //get rid of deprecation
            txtTitle.setTextColor(ContextCompat.getColor(getActivity(), R.color.main_color_grey_700));
            txtTitle.setText(title[i]);

            //add them to the views
            hl.addView(mdIconTxt);
            hl.addView(txtTitle);
            linearLayout.addView(ll);
        }
    }

    private void addProdBoxFeaturesList(LinearLayout linearLayout, String[] title){
        for (int i = 0; i < title.length; i++) {
            LinearLayout ll = new LinearLayout(getActivity());
            ll.setOrientation(LinearLayout.VERTICAL);

            LinearLayout hl = new LinearLayout(getActivity());
            hl.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            hl.setOrientation(LinearLayout.HORIZONTAL);
            ll.addView(hl);

            //create a material design text icon
            MaterialDesignIconsTextView mdIconTxt = new MaterialDesignIconsTextView(getActivity());
            ViewGroup.LayoutParams params = mdIconTxt.getLayoutParams();
            if (params != null) {
                params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            } else{
                params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
            mdIconTxt.setLayoutParams(params);
            mdIconTxt.setMinHeight(30);
            mdIconTxt.setPadding(8,8,8,8);
            mdIconTxt.setGravity(Gravity.RIGHT);
            mdIconTxt.setText(getResources().getString(R.string.material_icon_to_left));
            mdIconTxt.setTextSize(16);
            mdIconTxt.setTextColor(getResources().getColor(R.color.main_color_grey_700));
            //get rid of deprecation
            mdIconTxt.setTextColor(ContextCompat.getColor(getActivity(), R.color.main_color_grey_700));

            MyTextView txtTitle = new MyTextView(getActivity());
            ViewGroup.LayoutParams txtParams = txtTitle.getLayoutParams();
            if (txtParams != null) {
                txtParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                txtParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            } else{
                txtParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
            //lets set the margins
            txtTitle.setGravity(Gravity.CENTER_VERTICAL);
            txtTitle.setMinHeight(30);
            txtTitle.setPadding(8,8,8,8);
            txtTitle.setTextSize(14);
            txtTitle.setTextColor(getResources().getColor(R.color.main_color_grey_700));
            //get rid of deprecation
            txtTitle.setTextColor(ContextCompat.getColor(getActivity(), R.color.main_color_grey_700));
            txtTitle.setText(title[i]);

            //add them to the views
            hl.addView(mdIconTxt);
            hl.addView(txtTitle);
            linearLayout.addView(ll);
        }
    }
}
