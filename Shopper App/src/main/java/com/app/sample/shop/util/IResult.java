package com.app.sample.shop.util;

import com.android.volley.VolleyError;

import org.json.JSONObject;


/**
 * Created by shobo on 31/05/2017.
 */
public interface IResult {
    public void jsonObjNotifySuccess(String requestType, JSONObject response);
    public void stringRequestNotifySuccess(String requestType, String response);
    public void notifyError(String requestType, VolleyError error);
}
