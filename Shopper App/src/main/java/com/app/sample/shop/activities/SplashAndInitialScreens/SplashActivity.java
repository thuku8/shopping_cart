package com.app.sample.shop.activities.SplashAndInitialScreens;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE); //Removing ActionBar
        //setContentView(R.layout.activity_splash_screen);


        Intent intent = new Intent(this, SplashPagerActivity.class);
        startActivity(intent);
        finish();
    }
}
