package com.app.sample.shop.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.sample.shop.R;
import com.app.sample.shop.model.ItemModel;
import com.balysv.materialripple.MaterialRippleLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.ViewHolder> implements Filterable {

    private final int mBackground;
    private List<ItemModel> original_items = new ArrayList<>();
    private List<ItemModel> filtered_items = new ArrayList<>();
    private ItemFilter mFilter = new ItemFilter();

    private final TypedValue mTypedValue = new TypedValue();

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int position, ItemModel obj,TextView txtView);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title;
        public TextView category;
        public TextView price;
        public TextView total;
        public TextView vendor;
        public ImageView image;
        public ImageView img_inner_decrease;
        public ImageView img_inner_increase;
        public ImageView imgItemCartDelete;
        public TextView quantity_inner;

        public MaterialRippleLayout lyt_parent;

        public ViewHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.title);
            category = (TextView) v.findViewById(R.id.category);
            price = (TextView) v.findViewById(R.id.price);
            total = (TextView) v.findViewById(R.id.total);
            vendor = (TextView) v.findViewById(R.id.vendor);
            image = (ImageView) v.findViewById(R.id.image);
            img_inner_increase = (ImageView) v.findViewById(R.id.img_inner_increase);
            img_inner_decrease = (ImageView) v.findViewById(R.id.img_inner_decrease);
            imgItemCartDelete = (ImageView) v.findViewById(R.id.imgItemCartDelete);
            quantity_inner = (TextView) v.findViewById(R.id.quantity_inner);
            lyt_parent = (MaterialRippleLayout) v.findViewById(R.id.lyt_parent);
        }
    }

    public Filter getFilter() {
        return mFilter;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CartListAdapter(Context ctx, List<ItemModel> items) {
        this.ctx = ctx;
        original_items = items;
        filtered_items = items;
        ctx.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
    }

    @Override
    public CartListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart, parent, false);
        //v.setBackgroundResource(mBackground);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ItemModel p = filtered_items.get(position);
        holder.title.setText(p.getName());
        holder.category.setText(p.getCategory());
        holder.total.setText(p.getTotal()+" X");
        holder.price.setText(p.getStrPrice());
        if((p.getDealerName())== null || p.getDealerName().equals("")){
            String str= "Unspecified".toUpperCase();
            holder.vendor.setText("Vendor: "+str);
        }else{
            holder.vendor.setText("Vendor: "+p.getDealerName().toUpperCase());
        }
        Picasso.with(ctx).load(p.getImg())
                .resize(100,100)
                .into(holder.image);

        holder.quantity_inner.setText(p.getTotal() + "");
        // view detail message conversation
//        holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mOnItemClickListener != null) {
//                    mOnItemClickListener.onItemClick(view, position, p);
//                }
//            }
//        });

        for ( int i = 0; i < holder.lyt_parent.getChildCount();  i++ ){
            View view = holder.lyt_parent.getChildAt(i);
            //view.setVisibility(View.GONE); // Or whatever you want to do with the view.
            view.setClickable(true);
        }

        holder.img_inner_decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view, position, p,holder.quantity_inner);
                }
            }
        });

        holder.img_inner_increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view, position, p,holder.quantity_inner);
                }
            }
        });

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view, position, p,holder.quantity_inner);
                }
            }
        });



        holder.imgItemCartDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view, position, p,holder.quantity_inner);
                }
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String query = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();
            final List<ItemModel> list = original_items;
            final List<ItemModel> result_list = new ArrayList<>(list.size());

            for (int i = 0; i < list.size(); i++) {
                String str_title = list.get(i).getName();
                String str_cat = list.get(i).getCategory();
                if (str_title.toLowerCase().contains(query) || str_cat.toLowerCase().contains(query)) {
                    result_list.add(list.get(i));
                }
            }

            results.values = result_list;
            results.count = result_list.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filtered_items = (List<ItemModel>) results.values;
            notifyDataSetChanged();
        }

    }
}