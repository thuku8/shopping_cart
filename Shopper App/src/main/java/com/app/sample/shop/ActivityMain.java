package com.app.sample.shop;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.sample.shop.activities.authentication.ActivityLogin;
import com.app.sample.shop.data.GlobalVariable;
import com.app.sample.shop.data.Tools;
import com.app.sample.shop.fragment.CartFragment;
import com.app.sample.shop.fragment.MainHomeFragment;
import com.app.sample.shop.fragment.OrdersFragment;
import com.app.sample.shop.fragment.ProductsFragment;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class ActivityMain extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private Toolbar mToolbar;
    private LinearLayout linearLayoutToolbar;
    private ActionBarDrawerToggle mDrawerToggle;
    private ActionBar actionBar;
    private Menu menu;
    private View parent_view;
    private GlobalVariable global;
    private NavigationView nav_view;

    public static final String EXTRA_PRODUCT_ID = "product_id";
    public static final String EXTRA_PRODUCT_TITLE = "product_title";

    private static final String SAVED_PAGER_POSITION = "pager_position";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        parent_view = findViewById(R.id.main_content);
        global = (GlobalVariable) getApplication();

        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        }

        initToolbar();

        setupDrawerLayout();

        // display first page
        //displayView(R.id.nav_new, getString(R.string.menu_new));
        //actionBar.setTitle(R.string.app_name);

        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }

    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        linearLayoutToolbar = (LinearLayout) findViewById(R.id.linearLayoutToolbar);

        setSupportActionBar(mToolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }

    @Override
    protected void onResume() {
        updateChartCounter(nav_view, R.id.nav_cart, global.getCartItem());
        updateOrderCounter(nav_view, R.id.nav_orders, global.getOrdersItem());
        super.onResume();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void setupDrawerLayout() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view = (NavigationView) findViewById(R.id.navigation_view);
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                updateChartCounter(nav_view, R.id.nav_cart, global.getCartItem());
                updateOrderCounter(nav_view, R.id.nav_orders, global.getOrdersItem());
                super.onDrawerOpened(drawerView);
            }
        };
        // Set the drawer toggle as the DrawerListener
        drawerLayout.setDrawerListener(mDrawerToggle);
        updateChartCounter(nav_view, R.id.nav_cart, global.getCartItem());
        updateOrderCounter(nav_view, R.id.nav_orders, global.getOrdersItem());

        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                drawerLayout.closeDrawers();
                if (((String) menuItem.getTitle()).equals(getResources().getString(R.string.app_name))) {
                    actionBar.setTitle("");
                    displayView(menuItem.getItemId(), "","");
                }else{
                    actionBar.setTitle(menuItem.getTitle());
                    displayView(menuItem.getItemId(), menuItem.getTitle().toString(),"");
                }

                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.action_cart:
                displayView(R.id.nav_cart, getString(R.string.menu_cart),"");
                actionBar.setTitle(R.string.menu_cart);
                break;
            case R.id.action_login:
//                Intent intent = new Intent(ActivityMain.this, ActivityLogin.class);
//                startActivity(intent);

                ActivityLogin.navigate(this, parent_view.findViewById(R.id.action_login));
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void displayView(int id, String title,String hint) {
        actionBar.setDisplayShowCustomEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        Fragment fragment = null;
        Bundle bundle = new Bundle();
        Log.e("ID", String.valueOf(title));
        switch (id) {
            case R.id.nav_cart:
                fragment = new CartFragment();
                fragment.setArguments(bundle);
                linearLayoutToolbar.setVisibility(View.GONE);
                break;
            case R.id.nav_home:
                fragment = new MainHomeFragment();
                //bundle.putString(MainHomeFragment.TAG_CATEGORY, title);
                fragment.setArguments(bundle);
                linearLayoutToolbar.setVisibility(View.VISIBLE);
                break;
            case R.id.nav_new:
                fragment = new ProductsFragment();
                bundle.putString(ProductsFragment.TAG_CATEGORY, title);
                bundle.putString(ProductsFragment.TAG_SORT, hint);
                fragment.setArguments(bundle);
                linearLayoutToolbar.setVisibility(View.GONE);
                break;
            case R.id.nav_promo:
                fragment = new PromotionsFragment();
                fragment.setArguments(bundle);
                linearLayoutToolbar.setVisibility(View.GONE);
                break;
            case R.id.nav_orders:
                fragment = new OrdersFragment();
                fragment.setArguments(bundle);
                linearLayoutToolbar.setVisibility(View.GONE);
                break;
            case R.id.nav_clothing:
                fragment = new ProductsFragment();
                bundle.putString(ProductsFragment.TAG_CATEGORY, title);
                bundle.putString(ProductsFragment.TAG_SORT, hint);
                fragment.setArguments(bundle);
                linearLayoutToolbar.setVisibility(View.GONE);
                break;
            case R.id.nav_shoes:
                fragment = new ProductsFragment();
                bundle.putString(ProductsFragment.TAG_CATEGORY, title);
                bundle.putString(ProductsFragment.TAG_SORT, hint);
                fragment.setArguments(bundle);
                linearLayoutToolbar.setVisibility(View.GONE);
                break;
            case R.id.nav_watches:
                fragment = new ProductsFragment();
                bundle.putString(ProductsFragment.TAG_CATEGORY, title);
                bundle.putString(ProductsFragment.TAG_SORT, hint);
                fragment.setArguments(bundle);
                linearLayoutToolbar.setVisibility(View.GONE);
                break;
            case R.id.nav_accessories:
                fragment = new ProductsFragment();
                bundle.putString(ProductsFragment.TAG_CATEGORY, title);
                bundle.putString(ProductsFragment.TAG_SORT, hint);
                fragment.setArguments(bundle);
                linearLayoutToolbar.setVisibility(View.GONE);
                break;
            case R.id.nav_bags:
                fragment = new ProductsFragment();
                bundle.putString(ProductsFragment.TAG_CATEGORY, title);
                bundle.putString(ProductsFragment.TAG_SORT, hint);
                fragment.setArguments(bundle);
                linearLayoutToolbar.setVisibility(View.GONE);
                break;
            default:
                break;
        }

//        fragment.setArguments(bundle);

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_content, fragment);
//            fragmentTransaction.add(R.id.frame_content, fragment);
            fragmentTransaction.commit();
            //initToolbar();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent i = getIntent();
        Bundle b = i.getBundleExtra("launch_fragments");
        if (b != null) {
            String frag_name = b.getString("fragments");
            if(frag_name != null){
                if(frag_name.equals("launch_prod_details")){

                    String hintExtras = b.getString("hint");
                    String categoryExtras = b.getString("category");

                    if(categoryExtras.equals("New Items")){
                        actionBar.setTitle(categoryExtras);
                        displayView(R.id.nav_clothing, getString(R.string.menu_new),"");
                    }else{
                        actionBar.setTitle(hintExtras + categoryExtras);
                        String navString = "R.id.nav_"+categoryExtras.toLowerCase();
                        displayView(R.id.nav_clothing, categoryExtras,hintExtras);
                    }

                }else if(frag_name.equals("launch_cart")){
                    displayView(R.id.nav_cart, getString(R.string.menu_cart),"");
                    actionBar.setTitle(getString(R.string.menu_cart));
                }else if(frag_name.equals("launch_orders")){
                    displayView(R.id.nav_orders, getString(R.string.menu_orders),"");
                    actionBar.setTitle(getString(R.string.menu_orders));
                }else{
                    displayView(R.id.nav_home, getString(R.string.app_name),"");
                }
            }else{
                displayView(R.id.nav_home, getString(R.string.app_name),"");
            }
        }else{
            displayView(R.id.nav_home, getString(R.string.app_name),"");
        }

    }

    private long exitTime = 0;

    public void doExitApp() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(this, R.string.press_again_exit_app, Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        doExitApp();
    }

    private void updateChartCounter(NavigationView nav, @IdRes int itemId, int count) {
        TextView view = (TextView) nav.getMenu().findItem(itemId).getActionView().findViewById(R.id.counter);
        view.setText(String.valueOf(count));
    }

    private void updateOrderCounter(NavigationView nav, @IdRes int itemId, int count) {
        TextView view = (TextView) nav.getMenu().findItem(itemId).getActionView().findViewById(R.id.counter);
        view.setText(String.valueOf(count));
    }

}


