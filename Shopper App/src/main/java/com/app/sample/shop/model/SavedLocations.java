package com.app.sample.shop.model;

import java.io.Serializable;

/**
 * Created by shobo on 6/20/17.
 */

public class SavedLocations implements Serializable {

    //Data Variables
    private String savedPlaceName;
    private String googlePlaceName;
    private double latitude;
    private double longitude;


    public SavedLocations() {

    }


    public SavedLocations(String savedPlaceName,String googlePlaceName,double latitude,double longitude) {
        this.savedPlaceName = savedPlaceName;
        this.googlePlaceName = googlePlaceName;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getSavedPlaceName() {
        return savedPlaceName;
    }

    public void setSavedPlaceName(String savedPlaceName) {
        this.savedPlaceName = savedPlaceName;
    }

    public String getGooglePlaceName() {
        return googlePlaceName;
    }

    public void setGooglePlaceName(String googlePlaceName) {
        this.googlePlaceName = googlePlaceName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
