package com.app.sample.shop.util;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by shobo on 31/05/2017.
 */
public class VolleyService {

    IResult mResultCallback = null;
    Context mContext;

    VolleyService(IResult resultCallback, Context context){
        mResultCallback = resultCallback;
        mContext = context;
    }


    public void postDataVolley(final String requestType, String url, JSONObject sendObj){
        try {
            RequestQueue queue = Volley.newRequestQueue(mContext);

            JsonObjectRequest jsonObj = new JsonObjectRequest(url,sendObj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if(mResultCallback != null)
                        mResultCallback.jsonObjNotifySuccess(requestType,response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if(mResultCallback != null)
                        mResultCallback.notifyError(requestType,error);
                }
            });

            queue.add(jsonObj);

        }catch(Exception e){

        }
    }

    public void getDataVolley(final String requestType, String url){
        try {
            RequestQueue queue = Volley.newRequestQueue(mContext);

            JsonObjectRequest jsonObj = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if(mResultCallback != null)
                        mResultCallback.jsonObjNotifySuccess(requestType, response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if(mResultCallback != null)
                        mResultCallback.notifyError(requestType, error);
                }
            });

            queue.add(jsonObj);

        }catch(Exception e){

        }
    }


    //this is for dealing with string requests

    public void getDataStringRequest(final String requestType, String url){
        try {
            RequestQueue queue = Volley.newRequestQueue(mContext);

            StringRequest stringRequest = new StringRequest(Request.Method.GET,url,new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    //remember what is returned here is a string response and sshoulf be converted to a json object
                    //Do it with this it will work
                    //Convert the String response to JSONObject in the calling activity
                    if(mResultCallback != null)
                        mResultCallback.stringRequestNotifySuccess(requestType, response);
                }
            },new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if(mResultCallback != null)
                        mResultCallback.notifyError(requestType, error);
                }
            });

            queue.add(stringRequest);

        }catch(Exception e){

        }
    }

    //this is currently not working but will be sorted
    //Exeption Error org.json.JSONObject cannot be cast to java.util.Map

    public void postDataStringRequest(final String requestType, String url, final JSONObject sendObj){
        try {
            RequestQueue queue = Volley.newRequestQueue(mContext);

            StringRequest stringRequest = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if(mResultCallback != null)
                        mResultCallback.stringRequestNotifySuccess(requestType,response);
                }
            }, new Response.ErrorListener(){
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(mResultCallback != null)
                                mResultCallback.notifyError(requestType,error);
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams()
                {
                    //problem is here
                    //this is currently not working but will be sorted
                    //Exeption Error org.json.JSONObject cannot be cast to java.util.Map
                    Map<String, String> params = (Map<String, String>) sendObj;
                    return params;
                }
            };
            queue.add(stringRequest);

        }catch(Exception e){

        }
    }



}
