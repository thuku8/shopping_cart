package com.app.sample.shop;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.sample.shop.adapter.CardViewAdapter;
import com.app.sample.shop.adapter.CartListAdapter;
import com.app.sample.shop.data.GlobalVariable;
import com.app.sample.shop.data.Tools;
import com.app.sample.shop.fragment.ItemDescriptionFragment;
import com.app.sample.shop.fragment.ItemReviewsFragment;
import com.app.sample.shop.fragment.ItemSpecificationFragment;
import com.app.sample.shop.model.FeedProperties;
import com.app.sample.shop.model.ItemModel;
import com.app.sample.shop.util.DynamicHeightCustomPager;
import com.app.sample.shop.util.SliderChildAnimation;
import com.app.sample.shop.util.SliderLayout;
import com.app.sample.shop.widget.DividerItemDecoration;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by shobo on 5/18/17.
 */

public class ItemDetailsActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    public static final String EXTRA_OBJCT = "com.app.sample.shop.ITEM";

    // give preparation animation activity transition
    public static void navigate(AppCompatActivity activity, View transitionImage, ItemModel obj) {
        Intent intent = new Intent(activity, ItemDetailsActivity.class);
        intent.putExtra(EXTRA_OBJCT, obj);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, EXTRA_OBJCT);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    private ItemModel itemModel;
    private ActionBar actionBar;
    private GlobalVariable global;
    private View parent_view;
    private boolean in_cart=false;
    private ViewPager mViewPager;
    private ItemDescriptionFragment frag_itemDesc;
    private ItemSpecificationFragment frag_itemSpecs;
    private ItemReviewsFragment frag_itemReviews;
    private FloatingActionButton wishListFab;
    private SliderLayout prodSlider;

    TextView showMore,showLess,review;
    LinearLayout linear2;
    ImageView yellow1,yellow2,red2,red1,blue2,blue1,green2,green1;

    ImageView plus,minus;
    TextView sizeno;

    //recommended products
    private RecyclerView recommendedProdsRecyclerView;
    private ArrayList<FeedProperties> rec_products;
    private CardViewAdapter mRecProdsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_details_activity);
        parent_view = findViewById(android.R.id.content);

        // animation transition
        ViewCompat.setTransitionName(findViewById(R.id.prodSlider), EXTRA_OBJCT);

        global = (GlobalVariable) getApplication();

        // get extra object
        itemModel = (ItemModel) getIntent().getSerializableExtra(EXTRA_OBJCT);

        Log.e("ITEM ID", String.valueOf(itemModel.getId()));
        wishListFab = (FloatingActionButton) findViewById(R.id.wish_list_fab);

        showMore = (TextView)findViewById(R.id.showMore);
        showLess = (TextView) findViewById(R.id.showLess);

        linear2 = (LinearLayout)findViewById(R.id.linear2);

        //         ********Slider*********
        prodSlider = (SliderLayout)findViewById(R.id.prodSlider);
        //initialize products slider
        initProductsSlider();

        //********SIZE PLUS MINUS*************
        plus = (ImageView)findViewById(R.id.plus);
        minus = (ImageView)findViewById(R.id.minus);
        sizeno = (TextView) findViewById(R.id.sizeno);

        final int[] number = {0};
        sizeno.setText(""+ number[0]);

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (number[0] == 0){
                    sizeno.setText("" + number[0]);
                }

                if (number[0] > 0){

                    number[0] = number[0] -1;
                    sizeno.setText(""+ number[0]);
                }
            }
        });
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (number[0] == 9) {
                    sizeno.setText("" + number[0]);
                }

                if (number[0] < 9) {

                    number[0] = number[0] + 1;
                    sizeno.setText("" + number[0]);

                }
            }
        });

        initToolbar();

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(mViewPager);

        ((TextView) findViewById(R.id.title)).setText(itemModel.getName());
        ((TextView)findViewById(R.id.price)).setText(itemModel.getStrPrice());
        ((TextView)findViewById(R.id.vendorName)).setText("Vendor: "+itemModel.getDealerName());
        final Button bt_add_cart = (Button) findViewById(R.id.bt_add_cart);
        final Button bt_add_cart_2 = (Button) findViewById(R.id.bt_add_cart_2);
        final Button bt_buy_now = (Button) findViewById(R.id.bt_buy_now);
        final Button bt_buy_now_2 = (Button) findViewById(R.id.bt_buy_now_2);

        bt_add_cart.setClickable(false);
        bt_add_cart_2.setClickable(false);
        bt_buy_now.setClickable(false);
        bt_buy_now_2.setClickable(false);

        if(global.isCartExist(itemModel)){
            cartRemoveMode(bt_add_cart);
            cartRemoveMode(bt_add_cart_2);
        }

        showMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                linear2.setVisibility(View.VISIBLE);
                //linear1.setVisibility(View.GONE);

            }
        });

        showLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                linear2.setVisibility(View.GONE);
                //linear1.setVisibility(View.VISIBLE);


            }
        });

        bt_add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!in_cart){
                    global.addCart(itemModel);
                    cartRemoveMode(bt_add_cart);
                    cartRemoveMode(bt_add_cart_2);
                    Snackbar.make(view, "Added to Cart", Snackbar.LENGTH_SHORT).show();
                }else{
                    global.removeCart(itemModel);
                    crtAddMode(bt_add_cart);
                    crtAddMode(bt_add_cart_2);
                    Snackbar.make(view, "Removed from Cart", Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        bt_add_cart_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!in_cart){
                    global.addCart(itemModel);
                    cartRemoveMode(bt_add_cart);
                    cartRemoveMode(bt_add_cart_2);
                    Snackbar.make(view, "Added to Cart", Snackbar.LENGTH_SHORT).show();
                }else{
                    global.removeCart(itemModel);
                    crtAddMode(bt_add_cart);
                    crtAddMode(bt_add_cart_2);
                    Snackbar.make(view, "Removed from Cart", Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        bt_buy_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!in_cart){
                    global.addCart(itemModel);
                }else{
                    itemModel.setTotal(itemModel.getTotal() + 1);
                }
                in_cart = true;
                global.updateItemTotal(itemModel);

                Intent i = new Intent(ItemDetailsActivity.this, ActivityMain.class);
                Bundle b = new Bundle();
                b.putString("fragments", "launch_cart");
                i.putExtra("launch_fragments", b);
                startActivity(i);
            }
        });

        bt_buy_now_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!in_cart){
                    global.addCart(itemModel);
                }else{
                    itemModel.setTotal(itemModel.getTotal() + 1);
                }
                in_cart = true;
                global.updateItemTotal(itemModel);

                Intent i = new Intent(ItemDetailsActivity.this, ActivityMain.class);
                Bundle b = new Bundle();
                b.putString("fragments", "launch_cart");
                i.putExtra("launch_fragments", b);
                startActivity(i);
            }
        });

        wishListFab.setOnClickListener(new View.OnClickListener() {
            //@Override
            int counter = 0;
            public void onClick(View view) {
                counter ++;
                if(counter%2 == 1){
                    wishListFab.setImageResource(R.drawable.ic_nav_favorites);
                    Snackbar.make(parent_view, itemModel.getName() + " added to wishlist", Snackbar.LENGTH_SHORT).show();
                }else{
                    wishListFab.setImageResource(R.drawable.ic_nav_favorites_outline);
                    Snackbar.make(parent_view, itemModel.getName() + " removed from wish list", Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        recommendedProdsRecyclerView = (RecyclerView) findViewById(R.id.recommended_prods_recycler_view);
        initRecommendedProdsRecycler();
        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }

    private void initRecommendedProdsRecycler() {

        String[] versions = {};
        int[] icons = {};
        if(itemModel.getCategory().equals("Clothing") || itemModel.getCategory().equals("clothing")){

            versions = new String[]{"CLOTHING", "CLOTHING", "CLOTHING", "CLOTHING",
                    "CLOTHING", "CLOTHING"};
            icons = new int[]{R.drawable.c_f_1, R.drawable.c_m_2, R.drawable.c_f_3, R.drawable.c_m_4, R.drawable.c_f_5, R.drawable.c_m_7};

        }else if(itemModel.getCategory().equals("Shoes") || itemModel.getCategory().equals("shoes")){

            versions = new String[]{"SHOES", "SHOES", "SHOES", "SHOES",
                    "SHOES", "SHOES"};
            icons = new int[]{R.drawable.s_f_1, R.drawable.s_m_2, R.drawable.s_m_3, R.drawable.s_f_4, R.drawable.s_f_2, R.drawable.s_m_1};

        }else if(itemModel.getCategory().equals("Watches") || itemModel.getCategory().equals("watches")){

            versions = new String[]{"WATCHES", "WATCHES", "WATCHES", "WATCHES",
                    "WATCHES", "WATCHES"};
            icons = new int[]{R.drawable.w_m_1, R.drawable.w_f_2, R.drawable.w_m_3, R.drawable.w_f_4, R.drawable.w_m_5, R.drawable.w_m_6};

        }else if(itemModel.getCategory().equals("Accessories") || itemModel.getCategory().equals("accessories")){

            versions = new String[]{"ACCESSORIES", "ACCESSORIES", "ACCESSORIES", "ACCESSORIES",
                    "ACCESSORIES", "ACCESSORIES"};
            icons = new int[]{R.drawable.a_f_1, R.drawable.a_m_2, R.drawable.a_f_3, R.drawable.a_m_4, R.drawable.a_f_5, R.drawable.a_m_5};

        }else if(itemModel.getCategory().equals("Bags") || itemModel.getCategory().equals("bags")){

            versions = new String[]{"BAGS", "BAGS", "BAGS", "BAGS",
                    "BAGS", "BAGS"};
            icons = new int[]{R.drawable.b_f_1, R.drawable.b_m_2, R.drawable.b_f_3, R.drawable.b_m_4, R.drawable.b_f_6, R.drawable.b_m_6};

        }else{
            versions = new String[]{"NEW", "NEW", "NEW", "NEW",
                    "NEW", "NEW"};
            icons = new int[]{R.drawable.c_f_3, R.drawable.c_m_7, R.drawable.s_m_5, R.drawable.w_f_2, R.drawable.b_m_6, R.drawable.a_f_3};
        }


        rec_products = new ArrayList<FeedProperties>();

        for (int i = 0; i < versions.length; i++) {
            FeedProperties feed = new FeedProperties();

            feed.setTitle(versions[i]);
            feed.setThumbnail(icons[i]);
            rec_products.add(feed);
        }

        recommendedProdsRecyclerView.setHasFixedSize(true);

        // ListView
        recommendedProdsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //Grid View
        // recommendedProdsRecyclerView.setLayoutManager(new GridLayoutManager(this,2,1,false));

        //StaggeredGridView
        // recommendedProdsRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2,1));

        // create an Object for Adapter
        mRecProdsAdapter = new CardViewAdapter(rec_products);

        // set the adapter object to the recommendedProdsRecyclerView
        recommendedProdsRecyclerView.setAdapter(mRecProdsAdapter);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            linearLayoutManager.setOrientation(LinearLayout.HORIZONTAL);
        } else {
            linearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        }
        recommendedProdsRecyclerView.setLayoutManager(linearLayoutManager);
        recommendedProdsRecyclerView.setItemAnimator(new DefaultItemAnimator());


    }

    //         ********Slider*********
    private void initProductsSlider() {
        HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("1", itemModel.getImg());
        file_maps.put("2", R.drawable.w_m_1);
        file_maps.put("3",R.drawable.w_m_2);
        file_maps.put("4",R.drawable.w_m_3);


        for(String name : file_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    //  .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.CenterInside)
                    .setOnSliderClickListener(this);


            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", name);

            prodSlider.addSlider(textSliderView);
        }
        prodSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        prodSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        prodSlider.setCustomAnimation(new SliderChildAnimation());
        prodSlider.setDuration(2000);
        prodSlider.addOnPageChangeListener(this);

    }


    private void cartRemoveMode(Button bt){
        bt.setText("REMOVE FROM CART");
        bt.setBackgroundColor(getResources().getColor(R.color.colorRed));
        in_cart = true;
    }
    private void crtAddMode(Button bt){
        bt.setText("ADD TO CART");
        bt.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        in_cart = false;
    }

    private void setupViewPager(ViewPager mViewPager) {
        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager());

        if( frag_itemDesc == null ){ frag_itemDesc = new ItemDescriptionFragment(); }
        if( frag_itemSpecs == null ){ frag_itemSpecs = new ItemSpecificationFragment(); }
        if( frag_itemReviews == null ){ frag_itemReviews = new ItemReviewsFragment(); }

        adapter.addFragment(frag_itemDesc, "DESCRIPTION");
        adapter.addFragment(frag_itemSpecs, "SPECIFICATIONS");
        adapter.addFragment(frag_itemReviews, "REVIEWS");

        mViewPager.setAdapter(adapter);
    }



    static class MyPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        private int mCurrentPosition = -1;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            if (position != mCurrentPosition) {
                Fragment fragment = (Fragment) object;
                DynamicHeightCustomPager pager = (DynamicHeightCustomPager) container;
                if (fragment != null && fragment.getView() != null) {
                    mCurrentPosition = position;
                    pager.measureCurrentView(fragment.getView());
                }
            }
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_cart:
                dialogCartDetails();
                break;
            case R.id.action_settings:
                Snackbar.make(parent_view, "Setting Clicked", Snackbar.LENGTH_SHORT).show();
                break;
            case R.id.action_credit:
                Snackbar.make(parent_view, "Credit Clicked", Snackbar.LENGTH_SHORT).show();
                break;

            case R.id.action_about: {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("About");
                builder.setMessage(getString(R.string.about_text));
                builder.setNeutralButton("OK", null);
                builder.show();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }


    public void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(itemModel.getCategory());

    }

    private void dialogCartDetails() {

        final Dialog dialog = new Dialog(ItemDetailsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_cart_detail);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        LinearLayout lyt_notfound = (LinearLayout) dialog.findViewById(R.id.lyt_notfound);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));

        //set data and list adapter
        CartListAdapter mAdapter = new CartListAdapter(this, global.getCart());
        recyclerView.setAdapter(mAdapter);
        ((TextView)dialog.findViewById(R.id.item_total)).setText(" - " + global.getCartItemTotal() + " Items");
        ((TextView)dialog.findViewById(R.id.price_total)).setText(" $ " + global.getCartPriceTotal());
        ((ImageView)dialog.findViewById(R.id.img_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        if(mAdapter.getItemCount()==0){
            lyt_notfound.setVisibility(View.VISIBLE);
        }else{
            lyt_notfound.setVisibility(View.GONE);
        }
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void actionClick(View view){
        switch (view.getId()){
            case R.id.lyt_likes:
                Snackbar.make(view, "Likes Clicked", Snackbar.LENGTH_SHORT).show();
                break;
            case R.id.lyt_sales:
                Snackbar.make(view, "Sales Clicked", Snackbar.LENGTH_SHORT).show();
                break;
            case R.id.lyt_reviews:
                Snackbar.make(view, "Reviews Clicked", Snackbar.LENGTH_SHORT).show();
                break;

        }
    }


    @Override
    public void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        prodSlider.stopAutoCycle();
        super.onStop();
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
