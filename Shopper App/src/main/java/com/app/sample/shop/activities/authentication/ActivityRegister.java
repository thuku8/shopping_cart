package com.app.sample.shop.activities.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.app.sample.shop.R;
import com.app.sample.shop.font.MyLatoTextView;

/**
 * Created by shobo on 6/15/17.
 */

public class ActivityRegister extends AppCompatActivity {

    public static final String EXTRA_OBJCT = "com.app.sample.shop.activities.authentication.REGISTER";

    // give preparation animation activity transition
    public static void navigate(AppCompatActivity activity, View transitionImage) {
        Intent intent = new Intent(activity, ActivityRegister.class);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, EXTRA_OBJCT);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }


    MyLatoTextView signin1;
    private Toolbar mToolbar;
    private ActionBar actionBar;
    private View parent_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        parent_view = findViewById(R.id.main_content);

        // animation transition
        ViewCompat.setTransitionName(findViewById(R.id.logo), EXTRA_OBJCT);

        initToolbar();


        signin1 = (MyLatoTextView)findViewById(R.id.signin1);

        signin1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent it = new Intent(ActivityRegister.this, ActivityLogin.class);
//                startActivity(it);
//                finish();
                onBackPressed();
            }
        });
    }

    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.nunua_register);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
