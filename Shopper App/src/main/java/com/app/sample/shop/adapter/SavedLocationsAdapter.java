package com.app.sample.shop.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.sample.shop.R;
import com.app.sample.shop.data.SharedPreference;
import com.app.sample.shop.model.SavedLocations;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shobo on 6/20/17.
 */

public class SavedLocationsAdapter extends RecyclerView.Adapter<SavedLocationsAdapter.ViewHolder>{

    private final int mBackground;
    public List<SavedLocations> original_items = new ArrayList<SavedLocations>();
    public List<SavedLocations> filtered_items = new ArrayList<SavedLocations>();
    SharedPreference sharedPreference;

    private final TypedValue mTypedValue = new TypedValue();

    private Context ctx;
    private SavedLocationsAdapter.OnItemClickListener mOnItemClickListener;
    private SavedLocationsAdapter.OnItemClickListener mOnItemLongClickListener;



    public interface OnItemClickListener {
        void onItemClick(View view, int position, SavedLocations obj);
    }

    public void SetOnItemClickListener(final SavedLocationsAdapter.OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public void SetOnItemLongClickListener(final SavedLocationsAdapter.OnItemClickListener mItemLongClickListener) {
        this.mOnItemLongClickListener = mItemLongClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView savedLocationNameTxt;
        TextView googleLocationNameTxt;
        TextView latitudeTxt;

        public RelativeLayout lyt_parent;

        public ViewHolder(View convertView) {
            super(convertView);

            savedLocationNameTxt = (TextView) convertView
                    .findViewById(R.id.savedLocationNameTxt);
            googleLocationNameTxt = (TextView) convertView
                    .findViewById(R.id.googleLocationNameTxt);
            latitudeTxt = (TextView) convertView
                    .findViewById(R.id.latitudeTxt);

            lyt_parent = (RelativeLayout) convertView.findViewById(R.id.lyt_parent);
        }
    }


    // Provide a suitable constructor (depends on the kind of dataset)
    public SavedLocationsAdapter(Context ctx, List<SavedLocations> items) {
        this.ctx = ctx;
//        this.filtered_items = new ArrayList<SavedLocations>();
//        this.original_items = new ArrayList<SavedLocations>();
        original_items = items;
        filtered_items = items;
        ctx.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        sharedPreference = new SharedPreference();
    }

    @Override
    public SavedLocationsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.saved_locations_list_item, parent, false);
        //v.setBackgroundResource(mBackground);
        // set the view's size, margins, paddings and layout parameters
        SavedLocationsAdapter.ViewHolder vh = new SavedLocationsAdapter.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final SavedLocationsAdapter.ViewHolder holder, final int position) {
        final SavedLocations p = filtered_items.get(position);
        holder.savedLocationNameTxt.setText(p.getSavedPlaceName());
        holder.googleLocationNameTxt.setText("Address:"+p.getGooglePlaceName());
        holder.latitudeTxt.setText("Loc: ("+String.format("%.2f", p.getLatitude()) + ","+String.format("%.2f", p.getLongitude())+")");


        //  view detail message conversation
        holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view, position, p);
                }
            }
        });

        holder.lyt_parent.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (mOnItemLongClickListener != null) {
                    mOnItemLongClickListener.onItemClick(view, position, p);
                }
                return true;
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public void addItem(SavedLocations sv) {
        filtered_items.add(sv);
        notifyDataSetChanged();
    }

    public void remove(SavedLocations sv) {
        filtered_items.remove(sv);
        notifyDataSetChanged();
    }

}
