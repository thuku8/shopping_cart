package com.app.sample.shop.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.sample.shop.ActivityMain;
import com.app.sample.shop.R;
import com.app.sample.shop.activities.Orders.OrderDetailsActivity;
import com.app.sample.shop.adapter.OrdersListAdapter;
import com.app.sample.shop.data.GlobalVariable;
import com.app.sample.shop.model.Orders;
import com.app.sample.shop.widget.DividerItemDecoration;

import java.security.SecureRandom;

/**
 * Created by shobo on 6/7/17.
 */


public class OrdersFragment extends Fragment {
    private View view;
    private RecyclerView recyclerView;
    private GlobalVariable global;
    private OrdersListAdapter mAdapter;
    private TextView item_total, price_total;
    private LinearLayout lyt_notfound;
    private String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private SecureRandom rnd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_orders, null);
        global = (GlobalVariable) getActivity().getApplication();
        rnd = new SecureRandom();

        item_total = (TextView) view.findViewById(R.id.item_total);
        price_total = (TextView) view.findViewById(R.id.price_total);
        lyt_notfound = (LinearLayout) view.findViewById(R.id.lyt_notfound);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

        //set data and list adapter
        mAdapter = new OrdersListAdapter(getActivity(), global.getOrders());
        recyclerView.setAdapter(mAdapter);
        mAdapter.SetOnItemClickListener(new OrdersListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Orders model) {
                switch (view.getId()){
                    case R.id.lyt_parent:
                        OrderDetailsActivity.navigate((ActivityMain)getActivity(), view.findViewById(R.id.lyt_parent), model);
                        break;
                    default:
                        break;
                }
            }
        });


        if (mAdapter.getItemCount() == 0) {
            lyt_notfound.setVisibility(View.VISIBLE);
        } else {
            lyt_notfound.setVisibility(View.GONE);
        }
        return view;
    }

}
