package com.app.sample.shop.activities.SplashAndInitialScreens;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.app.sample.shop.ActivityMain;
import com.app.sample.shop.R;
import com.app.sample.shop.data.Tools;
import com.app.sample.shop.fragment.SplashPagerFragment;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.Timer;
import java.util.TimerTask;

public class SplashPagerActivity extends AppCompatActivity {

	private MyPagerAdapter adapter;
	private ViewPager pager;
	private TextView previousButton;
	private TextView nextButton;
    private TextView skipButton;
	private TextView navigator;
	private int currentItem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_pager);

		ImageLoader imageLoader = ImageLoader.getInstance();
		if (!imageLoader.isInited()) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(this));
		}

		currentItem = 0;

		pager = (ViewPager) findViewById(R.id.activity_wizard_universal_pager);
		previousButton = (TextView) findViewById(R.id.activity_wizard_universal_previous);
		nextButton = (TextView) findViewById(R.id.activity_wizard_universal_next);
        skipButton = (TextView) findViewById(R.id.activity_wizard_universal_skip);
		navigator = (TextView) findViewById(R.id.activity_wizard_universal_possition);

		adapter = new MyPagerAdapter(getSupportFragmentManager());
		pager.setAdapter(adapter);
		pager.setCurrentItem(currentItem);

		setNavigator();

		pager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

            @Override
            public void onPageScrollStateChanged(int position) {
                // TODO Auto-generated method stub
                if (pager.getCurrentItem() == 0) {
                    previousButton.setVisibility(View.INVISIBLE);
                } else {
                    previousButton.setVisibility(View.VISIBLE);
                }
                if (pager.getCurrentItem() == (pager.getAdapter().getCount() - 1)) {
                    nextButton.setText("Finish");
                } else {
                    nextButton.setText("Next");
                }
                setNavigator();
            }

		});

		previousButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
                // TODO Auto-generated method stub
                if (pager.getCurrentItem() != 0) {
                    pager.setCurrentItem(pager.getCurrentItem() - 1);
                }
                setNavigator();
			}
		});

		nextButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (pager.getCurrentItem() != (pager.getAdapter().getCount() - 1)) {
					pager.setCurrentItem(pager.getCurrentItem() + 1);
				} else {
					launchMainActivity();
				}
				setNavigator();
			}
		});

        skipButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                launchMainActivity();
            }
        });

        // for system bar in lollipop
        Tools.systemBarLolipop(this);

	}

	public void setNavigator() {
		String navigation = "";
		for (int i = 0; i < adapter.getCount(); i++) {
			if (i == pager.getCurrentItem()) {
				navigation += getString(R.string.material_icon_point_full)
						+ "  ";
			} else {
				navigation += getString(R.string.material_icon_point_empty)
						+ "  ";
			}
		}
		navigator.setText(navigation);
	}

	public void setCurrentSlidePosition(int position) {
		this.currentItem = position;
	}

	public int getCurrentSlidePosition() {
		return this.currentItem;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
	}

	public class MyPagerAdapter extends FragmentPagerAdapter {

		public MyPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return null;
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public Fragment getItem(int position) {
			if (position == 0) {
				return SplashPagerFragment.newInstance(position);
			} else if (position == 1) {
				return SplashPagerFragment.newInstance(position);
			} else {
				return SplashPagerFragment.newInstance(position);
			}
		}
	}


    //launnch the main activity
    private void launchMainActivity() {

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                // go to the main activity
                Intent i = new Intent(SplashPagerActivity.this, ActivityMain.class);
                startActivity(i);
                // kill current activity
                finish();
            }
        };
        // Show splash screen for 3 seconds
        new Timer().schedule(task, 100);


    }
}