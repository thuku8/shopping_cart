package com.app.sample.shop.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.sample.shop.ActivityMain;
import com.app.sample.shop.ExpandableListViewActivity;
import com.app.sample.shop.ItemDetailsActivity;
import com.app.sample.shop.R;
import com.app.sample.shop.adapter.FeaturedItemsGridAdapter;
import com.app.sample.shop.data.Constant;
import com.app.sample.shop.data.GlobalVariable;
import com.app.sample.shop.data.Tools;
import com.app.sample.shop.model.ItemModel;
import com.app.sample.shop.model.QuickOptionsGridItem;
import com.app.sample.shop.util.SliderLayout;
import com.app.sample.shop.widget.CircleTransform;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by shobo on 5/11/17.
 */

public class MainHomeFragment extends Fragment implements BaseSliderView.OnSliderClickListener, View.OnClickListener, ViewPagerEx.OnPageChangeListener  {

    public static String TAG = "com.app.sample.shop.MainHomeFragment";

//    View view;
    private RecyclerView recyclerView;
    private FeaturedItemsGridAdapter mAdapter;
    private LinearLayout lyt_notfound;
    private String category = "";
    private SliderLayout mDemoSlider;

    private GlobalVariable global;
    private boolean in_cart=false;

    private LinearLayout all_categories_ll,popular_ll,ready_food_ll,bread_cakes_ll,dairy_products_ll,groceries_ll,beverages_ll,fresh_meat_ll;
    private ImageView all_categories_iv,popular_iv,ready_food_iv,bread_cakes_iv,dairy_products_iv,groceries_iv,beverages_iv,fresh_meat_iv;

//    public MainHomeFragment() {
//        // Required empty public constructor
//    }


    View view;

//    @Override
//    public View onCreateView(LayoutInflater inflater,
//                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        setActionBar(null);

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        view = inflater.inflate(R.layout.main_home_fragment, null);

//        if(view==null){
//            view = inflater.inflate(R.layout.main_home_fragment, container, false);
//
//        }


//        if(view!=null){
//            if((ViewGroup)view.getParent()!=null)
//                ((ViewGroup)view.getParent()).removeView(view);
//            Log.e("BOOLEAN","Not null");
//            return view;
//        }else{
//            Log.e("BOOLEAN","null");
//        }
        view = inflater.inflate(R.layout.main_home_fragment, container, false);
        Log.e("BOOLEAN","null");

        global = (GlobalVariable) getActivity().getApplication();

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        lyt_notfound = (LinearLayout) view.findViewById(R.id.lyt_notfound);

        //the slider or image carousel
        mDemoSlider = (SliderLayout) view.findViewById(R.id.slider);

        setUpDemoSlider();

        //quick options initialize
        all_categories_ll = (LinearLayout) view.findViewById(R.id.all_categories_ll);
        all_categories_iv = (ImageView) view.findViewById(R.id.all_categories_iv);
        popular_ll = (LinearLayout) view.findViewById(R.id.popular_ll);
        popular_iv = (ImageView) view.findViewById(R.id.popular_iv);
        ready_food_ll = (LinearLayout) view.findViewById(R.id.ready_food_ll);
        ready_food_iv = (ImageView) view.findViewById(R.id.ready_food_iv);
        bread_cakes_ll = (LinearLayout) view.findViewById(R.id.bread_cakes_ll);
        bread_cakes_iv = (ImageView) view.findViewById(R.id.bread_cakes_iv);
        dairy_products_ll = (LinearLayout) view.findViewById(R.id.dairy_products_ll);
        dairy_products_iv = (ImageView) view.findViewById(R.id.dairy_products_iv);
        groceries_ll = (LinearLayout) view.findViewById(R.id.groceries_ll);
        groceries_iv = (ImageView) view.findViewById(R.id.groceries_iv);
        beverages_ll = (LinearLayout) view.findViewById(R.id.beverages_ll);
        beverages_iv = (ImageView) view.findViewById(R.id.beverages_iv);
        fresh_meat_ll = (LinearLayout) view.findViewById(R.id.fresh_meat_ll);
        fresh_meat_iv = (ImageView) view.findViewById(R.id.fresh_meat_iv);

        all_categories_ll.setOnClickListener(this);

        Picasso.with(getActivity()).load(R.drawable.all_categories) .resize(70, 70).transform(new CircleTransform()).into(all_categories_iv);

        LinearLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), Tools.getGridSpanCount(getActivity()));

        //StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(3,1);
        recyclerView.setLayoutManager(mLayoutManager);

        //set data and list adapter
        List<ItemModel> items = new ArrayList<>();

        items = Constant.getItemWatches(getActivity(),"All");

        mAdapter = new FeaturedItemsGridAdapter(getActivity(), items);
        recyclerView.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(recyclerView, false); //to disable nested scrolling for before and after API-21(Lollipop)
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new FeaturedItemsGridAdapter.OnItemClickListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onItemClick(View v, ItemModel obj, int position) {
                switch (v.getId()){
                    case R.id.image:
                        ItemDetailsActivity.navigate((ActivityMain)getActivity(), v.findViewById(R.id.image), obj);
                        break;
                    case R.id.addToCartNow:
                        addProductToCart(obj);
                        break;
                    case R.id.buyNowTxt:
                        addProductToCart(obj);
                        break;
                }
            }
        });

        if(mAdapter.getItemCount()==0){
            lyt_notfound.setVisibility(View.VISIBLE);
        }else{
            lyt_notfound.setVisibility(View.GONE);
        }

        return view;
    }

    /**
     * Prepare some dummy data for gridview
     */
    private ArrayList<QuickOptionsGridItem> getGridQuickMenuOptionsData() {

        final String[] gridItems = getResources().getStringArray(R.array.quick_options_grid_text);

        final ArrayList<QuickOptionsGridItem> imageItems;
        imageItems = new ArrayList<>();

        TypedArray imgs = getResources().obtainTypedArray(R.array.quick_options_grid_images);
        for (int i = 0; i < imgs.length(); i++) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), imgs.getResourceId(i, -1));
            imageItems.add(new QuickOptionsGridItem(bitmap, gridItems[i]));
        }



        return imageItems;
    }


    private void setUpDemoSlider() {
        HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("If you do,build a great experience", R.drawable.slid1);
        file_maps.put("Learn from your competitor,never copy.", R.drawable.slid2);
        file_maps.put("Create content that teaches", R.drawable.slid3);
        file_maps.put("Nunua,commerce leaders", R.drawable.slid4);


        for(String name : file_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);


            textSliderView.bundle(new Bundle());
            // textSliderView.getBundle().putString("extra",name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);
        mDemoSlider.addOnPageChangeListener(this);
    }


    @Override
    public void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Log.e("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    //add items to cart or buy now
    private void addProductToCart(ItemModel itemModel) {
        if(global.isCartExist(itemModel)){
            itemModel.setTotal(itemModel.getTotal() + 1);
        }else{
            global.addCart(itemModel);
        }
        in_cart = true;
        global.updateItemTotal(itemModel);

        Snackbar.make(view, "Added to Cart", Snackbar.LENGTH_SHORT).show();
    }

    private void cartRemoveMode(TextView txt){
//        txt.setText("REMOVE FROM CART");
//        txt.setBackgroundColor(getResources().getColor(R.color.colorRed));
        in_cart = true;
    }
    private void crtAddMode(TextView txt){
//        txt.setText("ADD TO CART");
//        txt.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        in_cart = false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.all_categories_ll:
                Intent i = new Intent(getActivity(),ExpandableListViewActivity.class);
                startActivity(i);
                break;
        }
    }
}
