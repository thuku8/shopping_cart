package com.app.sample.shop.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shobo on 6/7/17.
 */

public class Orders implements Serializable {


    private String orderId;
    private String date;
    private List<OrderProduct> orderItems = new ArrayList<OrderProduct>();
    private List<String> orderProducts = new ArrayList<String>();
    private String orderTotal;
    private String orderItemsTotal;

    long deliveryLat;
    long deliveryLong;
    String deliveryName;
    String distance;
    String paymentType;

    public Orders() {
    }

    public Orders(String orderId, String date) {
        this.orderId = orderId;
        this.date = date;
    }


    public Orders(String orderId, String date, List<OrderProduct> orderItems) {
        this.orderId = orderId;
        this.date = date;
        this.orderItems = orderItems;
    }

    public Orders(String orderId, String date,List<OrderProduct> orderItems,String orderTotal,String orderItemsTotal) {
        this.orderId = orderId;
        this.date = date;
        this.orderItems = orderItems;
        this.orderTotal = orderTotal;
        this.orderItemsTotal = orderItemsTotal;
    }

    public Orders(String orderId, String date,List<OrderProduct> orderItems,String orderTotal,String orderItemsTotal,long deliveryLat,
            long deliveryLong,
            String deliveryName,
            String distance,
            String paymentType
    ) {
        this.orderId = orderId;
        this.date = date;
        this.orderItems = orderItems;
        this.orderTotal = orderTotal;
        this.orderItemsTotal = orderItemsTotal;
        this.deliveryLat = deliveryLat;
        this.deliveryLong = deliveryLong;
        this.deliveryName = deliveryName;
        this.distance = distance;
        this.paymentType = paymentType;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public  List<OrderProduct> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderProduct> orderItems) {
        this.orderItems = orderItems;
    }


    public  List<String> getOrderProducts() {
        return orderProducts;
    }

    public void setOrderProducts(List<String> orderProducts) {
        this.orderProducts = orderProducts;
    }

    public String getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(String orderTotal) {
        this.orderTotal = orderTotal;
    }

    public String getOrderItemsTotal() {
        return orderItemsTotal;
    }

    public void setOrderItemsTotal(String orderTotal) {
        this.orderItemsTotal = orderItemsTotal;
    }

    public long getDeliveryLat() {
        return deliveryLat;
    }

    public void setDeliveryLat(long deliveryLat) {
        this.deliveryLat = deliveryLat;
    }

    public long getDeliveryLong() {
        return deliveryLong;
    }

    public void setDeliveryLong(long deliveryLong) {
        this.deliveryLong = deliveryLong;
    }

    public String getDeliveryName() {
        return deliveryName;
    }
    public void setDeliveryName(String deliveryName) {
        this.deliveryName = deliveryName;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getPaymentType() {
        return paymentType;
    }
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }



}
