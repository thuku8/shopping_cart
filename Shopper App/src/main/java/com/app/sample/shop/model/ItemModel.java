package com.app.sample.shop.model;

import java.io.Serializable;

public class ItemModel implements Serializable{
    long id;
    int img;
    String name;
    String dealerName;
    long priceOrig;
    long price;
    String category;
    long likes;
    int total=1;
    int priceCutPercent;

//    public ItemModel(){}

    public ItemModel(long id, int img, String name,long price, String category, long likes) {
        this.id = id;
        this.img = img;
        this.name = name;
        this.price = price;
        this.category = category;
        this.likes = likes;
    }


    public ItemModel(long id, int img, String name,String dealerName, long price,long priceOrig, String category, long likes) {
        this.id = id;
        this.img = img;
        this.name = name;
        this.dealerName = dealerName;
        this.price = price;
        this.priceOrig = priceOrig;
        this.category = category;
        this.likes = likes;
    }

    public long getId() {
        return id;
    }

    public int getImg() {
        return img;
    }

    public String getName() {
        return name;
    }

    public String getDealerName() {
        return dealerName;
    }

    public String getStrPrice() {
        return "Kshs "+getPrice();
    }

    public String getStrOrigPrice() {
        return "Kshs "+getPriceOrig();
    }

    public String getStrCutPricePercent() {
        return getPriceCutPercent()+"%";
    }

    public long getPrice() {
        return (price);
    }

    public long getPriceOrig() {
        return (priceOrig);
    }

    public int getPriceCutPercent() {

        return (int) Math.round(((priceOrig-price)));
        //return (int) Math.round((((priceOrig-price)/priceOrig)*100));
    }

    public long getSumPrice() {
        return (price*total);
    }

    public String getCategory() {
        return category;
    }
    public String getLikes() {
        if(likes > 100) {
            return "100+ Likes";
        }else{
            return likes+" Likes";
        }
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
