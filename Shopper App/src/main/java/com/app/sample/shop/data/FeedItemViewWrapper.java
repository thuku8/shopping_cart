package com.app.sample.shop.data;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.sample.shop.R;
import com.app.sample.shop.model.Feed;
import com.app.sample.shop.widget.CircleTransform;
import com.squareup.picasso.Picasso;

public class FeedItemViewWrapper {
    Activity activity;
    LinearLayout lyt_content;
    LinearLayout action_btns_lyt_content;
    LayoutInflater inflater;
    // construction
    public FeedItemViewWrapper(Activity activity, LinearLayout lyt_content) {
        this.activity = activity;
        this.lyt_content = lyt_content;
        inflater = LayoutInflater.from(activity);
        //removeAllView();
    }
    public void removeAllView() {
        lyt_content.removeAllViews();
    }

    // item view
    public void addItemFeed(Feed item) {
        View view = inflater.inflate(R.layout.item_list_feed, lyt_content, false);
        ImageView photo = (ImageView) view.findViewById(R.id.photo);
        action_btns_lyt_content = (LinearLayout) view.findViewById(R.id.action_btns_lyt_content);
        action_btns_lyt_content.setVisibility(View.GONE);
        ((TextView) view.findViewById(R.id.text_name)).setText(item.getFriend().getName());
        ((TextView) view.findViewById(R.id.text_date)).setText(item.getDate());
        Picasso.with(activity).load(R.drawable.ic_people) .resize(80, 80).transform(new CircleTransform()).into(photo);

        // content photo
        if(item.getPhoto()!=-1){
            ImageView feed_photo =  (ImageView) view.findViewById(R.id.photo_content);
            feed_photo.setVisibility(View.VISIBLE);
            feed_photo.setImageResource(item.getPhoto());
        }
        // content text
        if(item.getText()!=null){
            TextView text_content = (TextView) view.findViewById(R.id.text_content);
            text_content.setVisibility(View.VISIBLE);
            text_content.setText(item.getText());
        }


        ((ImageView) view.findViewById(R.id.bt_like)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Like Clicked", Snackbar.LENGTH_SHORT).show();
            }
        });
        ((ImageView) view.findViewById(R.id.bt_comment)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Comment Clicked", Snackbar.LENGTH_SHORT).show();
            }
        });
        ((ImageView) view.findViewById(R.id.bt_share)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Share Clicked", Snackbar.LENGTH_SHORT).show();
            }
        });
        ((ImageView) view.findViewById(R.id.bt_more)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "More Clicked", Snackbar.LENGTH_SHORT).show();
            }
        });

        lyt_content.addView(view);
    }

}
