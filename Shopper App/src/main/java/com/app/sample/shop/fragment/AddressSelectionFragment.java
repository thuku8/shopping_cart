package com.app.sample.shop.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.app.sample.shop.ExpandableListViewActivity;
import com.app.sample.shop.R;
import com.app.sample.shop.activities.Payments.ActivityCheckOut;
import com.app.sample.shop.adapter.SavedLocationsAdapter;
import com.app.sample.shop.data.GlobalVariable;
import com.app.sample.shop.data.SharedPreference;
import com.app.sample.shop.font.MyLatoEditText;
import com.app.sample.shop.model.SavedLocations;
import com.app.sample.shop.widget.DividerItemDecoration;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by shobo on 6/8/17.
 */

public class AddressSelectionFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks,View.OnClickListener{
    private View view;
    private GlobalVariable global;


    private static final String TAG = "AddressSelection";

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final float DEFAULT_ZOOM = 12.0f;

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;

    private CameraPosition mCameraPosition;
    private LatLng mDefaultLocation = new LatLng(-1.2630112, 36.8417109);
    private Location mLastKnownLocation;
    private LatLng mSelectedLocation;

    private boolean mLocationPermissionGranted;

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";

    private Button useCurrentBtn,add_new_location,bt_save_location,bt_proceed_to_payment,bt_previous_locations;
    private Button dialogButtonSave,dialogButtonCancel;
    private MyLatoEditText addressToSaveEt;
    private SupportMapFragment mapFragment;
    private Bundle bundle;
    private String placeName;
    private LinearLayout actionsLinearLayout,lyt_not_found,lyt_found;

    private RecyclerView savedLocationsRecyclerView;
    private SharedPreference sharedPreference;
    private List<SavedLocations> savedLocations;
    //lets get the location name
//    GPSTracker gps;
    private Geocoder geocoder;
    private List<Address> addresses;
    private Activity activity;
    private SavedLocationsAdapter savedLocationsAdapter;
    private Dialog saveLocationsDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        view = inflater.inflate(R.layout.activity_address_selection, null);
        view = inflater.inflate(R.layout.activity_address_selection, null, false);
        global = (GlobalVariable) getActivity().getApplication();

        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        //set action bar title
        ((ActivityCheckOut) getActivity()).setActionBarTitle(ActivityCheckOut.PAYMENT_ADDRESS_STRING);

        //initialize autocomplete
        initAutoComplete();

        //initialize all views
        useCurrentBtn = (Button) view.findViewById(R.id.use_current);
        lyt_not_found = (LinearLayout) view.findViewById(R.id.lyt_not_found);
        actionsLinearLayout = (LinearLayout) view.findViewById(R.id.actionsLinearLayout);
        lyt_found = (LinearLayout) view.findViewById(R.id.lyt_found);
        add_new_location = (Button) view.findViewById(R.id.add_new_location);
//        bt_save_location = (Button) view.findViewById(R.id.bt_save_location);
        bt_previous_locations = (Button) view.findViewById(R.id.bt_previous_locations);
        bt_proceed_to_payment = (Button) view.findViewById(R.id.bt_proceed_to_payment);

        //we are fetching the locations that are saved
        // Get location items from SharedPreferences.
        sharedPreference = new SharedPreference();
        savedLocations = sharedPreference.getLocations(activity);


        initRecyclerView();

        if (savedLocations == null) {
            lyt_not_found.setVisibility(View.VISIBLE);
            lyt_found.setVisibility(View.GONE);
            add_new_location.setVisibility(View.GONE);
            useCurrentBtn.setVisibility(View.VISIBLE);
            actionsLinearLayout.setVisibility(View.GONE);

        } else {

            if (savedLocations.size() == 0) {
                lyt_not_found.setVisibility(View.VISIBLE);
                lyt_found.setVisibility(View.GONE);
                add_new_location.setVisibility(View.GONE);
                useCurrentBtn.setVisibility(View.VISIBLE);
                actionsLinearLayout.setVisibility(View.GONE);
            }else{

//                initRecyclerView();

                //on address item click
                savedLocationsAdapter.SetOnItemClickListener(new SavedLocationsAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position, SavedLocations model) {

                        switch (view.getId()){
                            case R.id.lyt_parent:
                                calculateDistance(model.getLatitude(),model.getLongitude(),model.getGooglePlaceName());
                                break;
                            default:
                                break;
                        }
                    }
                });

                //on long address item delete the address
                savedLocationsAdapter.SetOnItemLongClickListener(new SavedLocationsAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position, SavedLocations model) {

                        switch (view.getId()){
                            case R.id.lyt_parent:

                                sharedPreference.removeLocation(activity,
                                        model,position);
                                savedLocationsAdapter.remove(savedLocations
                                        .get(position));
                                Toast.makeText(activity,"The address was deleted!!!",Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                break;
                        }
                    }
                });

//
                if (savedLocationsAdapter.getItemCount() == 0) {
                    lyt_not_found.setVisibility(View.VISIBLE);
                    lyt_found.setVisibility(View.GONE);
                    add_new_location.setVisibility(View.GONE);
                    useCurrentBtn.setVisibility(View.VISIBLE);
                    actionsLinearLayout.setVisibility(View.GONE);
                } else {
                    lyt_not_found.setVisibility(View.GONE);
                    lyt_found.setVisibility(View.VISIBLE);
                    add_new_location.setVisibility(View.VISIBLE);
                    useCurrentBtn.setVisibility(View.GONE);
                    actionsLinearLayout.setVisibility(View.GONE);
                }
            }
        }


        //show the map since we want to add a new location and possibly add it to saved locations
        add_new_location.setOnClickListener(this);
        //use the current location and possibly add it to saved locations
        useCurrentBtn.setOnClickListener(this);
        bt_proceed_to_payment.setOnClickListener(this);
        bt_previous_locations.setOnClickListener(this);

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity(), this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();


        return view;
    }

    private void initRecyclerView() {
        lyt_not_found.setVisibility(View.GONE);
        lyt_found.setVisibility(View.VISIBLE);
        add_new_location.setVisibility(View.VISIBLE);
        useCurrentBtn.setVisibility(View.GONE);
        actionsLinearLayout.setVisibility(View.GONE);

        savedLocationsRecyclerView = (RecyclerView) view.findViewById(R.id.savedLocationsRecyclerView);

        savedLocationsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

//        savedLocationsRecyclerView.setHasFixedSize(true);
        savedLocationsRecyclerView.setHasFixedSize(true);
        savedLocationsRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

        //set data and list adapter
        savedLocationsAdapter = new SavedLocationsAdapter(activity, savedLocations);
        savedLocationsRecyclerView.setAdapter(savedLocationsAdapter);
        savedLocationsAdapter.notifyDataSetChanged();

    }

    private void initAutoComplete() {
        PlaceAutocompleteFragment autocompleteFragment;
        autocompleteFragment = (PlaceAutocompleteFragment) getActivity().
                getFragmentManager().findFragmentById(R.id.selected_address);

        autocompleteFragment.setHint("Search Location");
        //((EditText)autocompleteFragment.getView().findViewById(R.id.selected_address)).setTextSize(10.0f);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                placeName = String.valueOf(place.getName());
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(place.getLatLng()).title((String) place.getName()).draggable(true));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 12.0f));

                mSelectedLocation = place.getLatLng();

                ///now make these views visible or not
//                actionsLinearLayout.setVisibility(View.VISIBLE);
//                useCurrentBtn.setVisibility(View.GONE);
                //proceed.setVisibility(View.VISIBLE);
                showSaveLocationsDialog(activity);

            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.e(TAG, "An error occurred: " + status);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMyLocationEnabled(true); // false to disable

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(latLng).title(latLng.toString()).draggable(true));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12.0f));

                mSelectedLocation = latLng;
                //needs more research
                showSaveLocationsDialog(activity);
            }
        });

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        getDeviceLocation();


    }


    public static double getDistanceBetweenLocations(LatLng startLatLng, LatLng endLatLng) {

        int Radius = 6371;// radius of earth in Km

        double startLat = startLatLng.latitude;
        double startLng = startLatLng.longitude;

        double endLat = endLatLng.latitude;
        double endLng = endLatLng.longitude;

        double dLat = Math.toRadians(endLat - startLat);
        double dLon = Math.toRadians(endLng - startLng);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(startLat))
                * Math.cos(Math.toRadians(endLat)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);

        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;

        DecimalFormat newFormat = new DecimalFormat("####");

        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));

//        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
//                + " Meter   " + meterInDec);

        return Math.round(Radius * c);
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
    }

    private void getDeviceLocation() {
        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

        if (mLocationPermissionGranted) {
            mLastKnownLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);
        }

        // Set the map's camera position to the current location of the device.
        if (mCameraPosition != null) {
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
        } else if (mLastKnownLocation != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(mLastKnownLocation.getLatitude(),
                            mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
        } else {
            Log.e(TAG, "Current location is null. Using defaults.");
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }

        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

        if (mLocationPermissionGranted) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        } else {
            mMap.setMyLocationEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mLastKnownLocation = null;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private void calculateDistance(double latitude, double longitude, String googlePlaceName) {

        LatLng start;
        LatLng end;
        double distance;

        start = new LatLng(latitude, longitude);
        end = new LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude);
        distance = getDistanceBetweenLocations(start, end);

        placeName = googlePlaceName;
        callNextFragment(latitude,longitude,distance);
    }

    public void showSaveLocationsDialog(Activity activity){
        saveLocationsDialog= new Dialog(activity);
        saveLocationsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        saveLocationsDialog.setCancelable(false);
        saveLocationsDialog.setContentView(R.layout.dialog_save_location);

        dialogButtonSave = (Button) saveLocationsDialog.findViewById(R.id.btn_dialog_save);
        dialogButtonCancel = (Button) saveLocationsDialog.findViewById(R.id.btn_dialog_cancel);
        addressToSaveEt = (MyLatoEditText) saveLocationsDialog.findViewById(R.id.addressToSaveEt);

        dialogButtonSave.setOnClickListener(this);

        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveLocationsDialog.dismiss();
                actionsLinearLayout.setVisibility(View.VISIBLE);
                useCurrentBtn.setVisibility(View.GONE);
            }
        });

        saveLocationsDialog.show();

    }

    private boolean checkIfLocationExists(double latitude, double longitude) {

        List<SavedLocations> favorites = sharedPreference.getLocations(getActivity());

        boolean check = false;

        if (favorites != null) {
            for (SavedLocations product : favorites) {
                //if (product.equals(checkProduct)) {
                if (product.getLatitude() ==latitude && product.getLongitude() == longitude) {
                    check = true;
                }
            }
        }
        return check;
    }

    public void proceed(View view) {
        /* For Calculating Distance */
        LatLng start;
        LatLng end;
        double latitude;
        double longitude;
        double distance;

        if(mSelectedLocation == null){
            start = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
            end = new LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude);
            latitude = mLastKnownLocation.getLatitude();
            longitude = mLastKnownLocation.getLongitude();
            distance = getDistanceBetweenLocations(start, end);
        }else{
            start = new LatLng(mSelectedLocation.latitude, mSelectedLocation.longitude);
            end = new LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude);
            latitude = mSelectedLocation.latitude;
            longitude = mSelectedLocation.longitude;
            distance = getDistanceBetweenLocations(start, end);
        }
        if(placeName == null){
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);

                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                placeName = address+","+city;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        callNextFragment(latitude,longitude,distance);

    }

    private void callNextFragment(double latitude, double longitude, double distanceBetweenLocations) {
        Fragment fragment = null;
        bundle = new Bundle();

        fragment = new PaymentTypesFragment();
        bundle.putString(ActivityCheckOut.TAG_LONG, String.valueOf(latitude));
        bundle.putString(ActivityCheckOut.TAG_LAT, String.valueOf(longitude));
        bundle.putString(ActivityCheckOut.TAG_PLACE_DISTANCE,String.valueOf(distanceBetweenLocations));
        if(placeName == null){
            placeName = "Chosen Address";
        }
        bundle.putString(ActivityCheckOut.TAG_PLACE_NAME, String.valueOf(placeName));
        fragment.setArguments(bundle);

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_content, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onResume() {
        ((ActivityCheckOut) getActivity()).setActionBarTitle(ActivityCheckOut.PAYMENT_ADDRESS_STRING);
        super.onResume();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_new_location:
                lyt_not_found.setVisibility(View.VISIBLE);
                lyt_found.setVisibility(View.GONE);
                add_new_location.setVisibility(View.GONE);
                useCurrentBtn.setVisibility(View.VISIBLE);
                actionsLinearLayout.setVisibility(View.GONE);
                break;
            case R.id.use_current:
                showSaveLocationsDialog(activity);
                break;
            case R.id.bt_proceed_to_payment:
                proceed(v);
                break;
            case R.id.bt_previous_locations:
                lyt_not_found.setVisibility(View.GONE);
                lyt_found.setVisibility(View.VISIBLE);
                add_new_location.setVisibility(View.VISIBLE);
                useCurrentBtn.setVisibility(View.GONE);
                actionsLinearLayout.setVisibility(View.GONE);
                break;
            case R.id.btn_dialog_save:
                double latitude;
                double longitude;
                String address = null;
                String city = "";
                SavedLocations sv;

                if(mSelectedLocation != null){
                    latitude = mSelectedLocation.latitude;
                    longitude = mSelectedLocation.longitude;
                }else if(mLastKnownLocation !=null) {
                    latitude = mLastKnownLocation.getLatitude();
                    longitude = mLastKnownLocation.getLongitude();
                }else{
                    latitude = mDefaultLocation.latitude;
                    longitude = mDefaultLocation.longitude;
                }

                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                    address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    city = addresses.get(0).getLocality();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                //check if the location exists dont save
                if(checkIfLocationExists(latitude,longitude)){
                    Toast.makeText(getActivity(),"The address exists and was not saved!!!",Toast.LENGTH_SHORT).show();
                }else{
                    sv = new SavedLocations(addressToSaveEt.getText().toString(),address+","+city,latitude,longitude);

                    sharedPreference = new SharedPreference();
                    sharedPreference.addLocation(getActivity().getApplicationContext(),sv);
                    savedLocationsAdapter.addItem(sv);

                }

                saveLocationsDialog.dismiss();
                lyt_not_found.setVisibility(View.VISIBLE);
                lyt_found.setVisibility(View.GONE);
                add_new_location.setVisibility(View.GONE);
                useCurrentBtn.setVisibility(View.GONE);
                actionsLinearLayout.setVisibility(View.VISIBLE);
                break;
        }
    }
}
